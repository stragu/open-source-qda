# Open Source QDA

Information about Open Source tools for Qualitative Data Analysis

## Motivation

I (Stéphane Guillou) am not a regular user of Qualitative Data Analysis methods and tools, but I have developped a keen interest in finding Open Source alternative to existing proprietary tools.

This repository, open to contributions, is an attempt at gathering information on the topic.

## Tools

This list is an overview of what tools are currently avaiable:

* [Taguette](https://www.taguette.org/) by Remi Rampin, Vicky Rampin, Sarah DeMott and contributors. Latest release: [version 1.1.1](https://gitlab.com/remram44/taguette/-/releases#v1.1.1) (2021-10-19)
* [QualCoder](https://qualcoder.wordpress.com/) by Colin Curtain and contributors. Latest release: [version 2.8](https://github.com/ccbogel/QualCoder/releases/tag/2.8) (2021-10-20)
* [RQDA](https://github.com/Ronggui/RQDA) (R package). Original developer seems to have half-abandoned the project, but [others](https://github.com/RQDA/RQDA) have been trying to keep it alive.
* [qcoder](https://docs.ropensci.org/qcoder/) (R package)
* [CATMA](https://catma.de/) by Evelyn Gius, Mareike Schumacher, Dominik Gerstorfer, Malte Meister and former team members.

### Defunct

This list keeps track of defunct project (abandonned, discontinued or disfunctional for whatever reason). Keeping track of them is also important as, being Open Source, the projects might be revived by keen developers.

* [Weft QDA](http://www.pressure.to/qda/) ([Web Archive link](https://web.archive.org/web/20210719052108/http://www.pressure.to/qda/)), last updated 2006-04-26
* [CAT](https://cat.texifter.com/) ([Web Archive link](https://web.archive.org/web/20210814133126/http://cat.texifter.com/)), officially decommissioned on 2020-09-13

## Links

This section links to other useful resources on the topic of Open Source QDA:

* AlternativeTo.net can be used to look for Open Source alternatives to proprietary software. For example, see [Open Source alternatives to NVivo](https://alternativeto.net/software/nvivo/?license=opensource)
* [CAQDAS webinar 016: Open Tools for Qualitative Analysis](https://www.youtube.com/watch?v=9teZO-KVwqk) – great overview of the FLOSS CAQDAS landscape, by Vicky Rampin, focusing on Qualcoder, Taguette and qcoder. (2021-06-09)
